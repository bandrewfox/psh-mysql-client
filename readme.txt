
Welcome to psh-mysql-client

---- more info to be added later -----


----  first, install psh -----
To install, you first need to install Perl Shell (psh) itself from:
http://search.cpan.org/~gregor/psh-1.8.1/

and to get that working nicely, you will need the ReadLine package, as
it explains in the documentation for psh.

(On Debian systems, do this to get ReadLine:
> sudo apt-get install libterm-readline-gnu-perl
I think there is a package for psh, but I didn't try that)

---- then copy these files to the right place ----

After you have psh working, you move the 3 perl module files:
                Sql.pm Db.pm Dbcore.pm

to specific directories wherever psh ended up.  For example:
sudo cp Sql.pm    /usr/local/share/perl/5.8.8/Psh
sudo cp Db.pm     /usr/local/share/perl/5.8.8/Psh/Strategy/
sudo cp Dbcore.pm /usr/local/share/perl/5.8.8/Psh/Builtins/

---- Modify ~/.pshrc --------
then, add the contents of pshrc to your ~/.pshrc file

---- End of Installation ----

Thank you for your interest and please let me know if you 
want to make commits to the code, I am happy to have help
since I am not a professionally trained programmer


